## About me:

- Born in the late 70s. Studied computer science. Working as a full-time JEE developer.
- Interests: Data privacy, open source, Linux, games (PC games, RPG, tabletops), emulation, retrogaming, nature, photography, movies (fantasy, science fiction, horror), music (metal, rock, electronic, SID)
- Languages: German & English

## Hobby projects:

- Writing [bash](https://codeberg.org/Stoabrogga/bash) scripts and [docs](https://codeberg.org/Stoabrogga/docs)
- Since 2017: World of Warcraft server emulation and a bit [modding](https://www.wowmodding.net/profile/16659-stoabrogga/content/?type=downloads_file)
  - 2017-2018: Played around with repacks created by [conan513](https://github.com/conan513) and then compiled and installed my own [AzerothCore](https://github.com/azerothcore) server
  - 2018-2020: Contributed over 200 commits to AzerothCore
  - 2020-2024: Project ["Sol"](https://opfesoft.gitlab.io/)
    - Quit AzerothCore and started working on my own server project "Sol" in March 2020, free and without restrictions from other projects
    - Stopped working on Sol in July 2024 because I realized that I did like the old world the most, so a server software supporting only vanilla would have been better suited in the first place
    - Switched to [VMaNGOS](https://github.com/vmangos)
  - Since 2024:
    - Installed VMaNGOS to play on LAN, see [readme](https://codeberg.org/Stoabrogga/docs/src/branch/root/vmangos#readme)
    - Maintain my own [VMaNGOS fork](https://codeberg.org/Stoabrogga/vmangos) with a few customizations
- Since 2019: [Minecraft](https://www.minecraft.net/) LAN/internet server, see [readme](https://codeberg.org/Stoabrogga/docs/src/branch/root/minecraft#readme)
- Since 2021: [Luanti](https://www.luanti.org/) (aka "Minetest") LAN/internet server
- 2004-2021: Maintenance & administration of an internet root server (Ubuntu) providing a few services:
  - Movie DB using PHP & MySQL
  - [phpBB2/3 forum](https://www.phpbb.com/)
  - Server software to play multiplayer games with my friends (Killing Floor 2, Minecraft, Avorion etc.) running in [schroot](https://wiki.debian.org/Schroot), see [docs](https://codeberg.org/Stoabrogga/docs/src/branch/root/schroot.md)
  - [XMPP](https://xmpp.org/) server using [Prosody](https://prosody.im/), see [docs](https://codeberg.org/Stoabrogga/docs/src/branch/root/xmpp_server_using_prosody.md)
  - [Mumble](https://www.mumble.info/) server as a replacement for TeamSpeak
- 2021: Finally shut down the internet root server because it was getting too expensive and was not used that much anymore

## Computer history:

- Early/mid 80s:
  - [Universum Color Multispiel 4010](https://www.gametechwiki.com/w/index.php?title=Universum_TV_Multispiel&mobileaction=toggle_view_desktop#Universum_Color_Multispiel_4010)
  - [Atari 2600](https://en.wikipedia.org/wiki/Atari_2600)
- Mid/late 80s:
  - [ZX81](https://en.wikipedia.org/wiki/ZX81)
- Early 90s:
  - [C64](https://www.c64-wiki.com/wiki/Video_Supergame_64)
- Mid 90s:
  - 386 PC and many more PCs to follow

## Accounts:

- Active:
  - `stoabrogga@proton.me`
- Inactive:
  - `38475780+Stoabrogga@users.noreply.github.com` (used for AC commits from 2018-05-03 to 2020-02-28)
  - `5532995-Stoabrogga@users.noreply.gitlab.com` (used for Sol commits from 2020-03-01 to 2023-06-24)

